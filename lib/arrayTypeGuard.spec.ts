import { arrayTypeGuard } from './arrayTypeGuard';
import { isNumber } from './isNumber';
import { isString } from './isString';

describe('arrayTypeGuard', () => {
    const stringVariable = 'string';
    const numberVariable = 1;

    it('should validate arrays', () => {
        expect(isString(stringVariable)).toBeTruthy();
        expect(isNumber(stringVariable)).toBeFalsy();

        expect(isString(numberVariable)).toBeFalsy();
        expect(isNumber(numberVariable)).toBeTruthy();

        const numberArrayTypeguard = arrayTypeGuard(isNumber);
        expect(numberArrayTypeguard([numberVariable, numberVariable])).toBeTruthy();
        expect(numberArrayTypeguard([numberVariable, stringVariable])).toBeFalsy();
    });

    it('should never validate objects that are not arrays', () => {
        const numberArrayTypeguard = arrayTypeGuard(isNumber);
        expect(numberArrayTypeguard(stringVariable)).toBeFalsy();
        expect(numberArrayTypeguard(numberVariable)).toBeFalsy();
    });
});
