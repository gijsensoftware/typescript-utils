export const valuesFromEnum = (enumerable: any) => Object.keys(enumerable)
    .filter((item: string) => isNaN(Number(item)))
    .map((key: string) => enumerable[key]);
