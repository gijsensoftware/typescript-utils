import { isArray } from './isArray';
import { OBJECT } from './types';

export const isNonNullNonArrayObject = (input: unknown): input is { [key: string]: any } =>
    typeof input === OBJECT
    && !isArray(input)
    && input !== null;
