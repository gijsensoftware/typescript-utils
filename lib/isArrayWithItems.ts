import { TypeGuard } from './typeGuard';
import { OBJECT } from './types';

export const isArrayWithItems = <T>(guard: TypeGuard<T>,
                                    input: unknown): input is T[] =>
    typeof input === OBJECT
    && Array.isArray(input)
    && input.every((item: unknown) => guard(item))
