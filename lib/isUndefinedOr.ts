import { isUndefined } from './isUndefined';
import { TypeGuard } from './typeGuard';

export const isUndefinedOr = <T>(typeCheck: TypeGuard<T>, input: unknown): input is undefined | T => isUndefined(input) || typeCheck(input)
