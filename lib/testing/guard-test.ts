import { isString } from '../isString';
import { TypeGuard } from '../typeGuard';

export const testGuard = <T>(guard: TypeGuard<T>, input: T[]) => {
    if (input.length === 0) {
        throw Error('Can not run guard test without input elements');
    }

    input
        .forEach((item: T,
                  index: number) => {
            const label = isString(item) && item.length <= 20 ? '"' + item + '"' : (index + 1);
            it('should validate input ' + label, () => {
                expect(guard(item))
                    .toStrictEqual(true);
            });
        });

    it('should not validate strings', () => expect(guard('')).toStrictEqual(false));
    it('should not validate numbers', () => {
        expect(guard(0)).toStrictEqual(false);
        expect(guard(1)).toStrictEqual(false);
        expect(guard(-1)).toStrictEqual(false);
        expect(guard(0.00001)).toStrictEqual(false);
    });
};
