import { isNonNullNonArrayObject } from './isNonNullNonArrayObject';

describe('isNonNullNonArrayObject', () => {
    it('should validate objects', () => {
        expect(isNonNullNonArrayObject({})).toStrictEqual(true);
        expect(isNonNullNonArrayObject({foo: 'bar'})).toStrictEqual(true);
    });

    it('should not validate non-objects', () => {
        expect(isNonNullNonArrayObject(undefined)).toStrictEqual(false);
        expect(isNonNullNonArrayObject(null)).toStrictEqual(false);
        expect(isNonNullNonArrayObject(1)).toStrictEqual(false);
        expect(isNonNullNonArrayObject('')).toStrictEqual(false);
        expect(isNonNullNonArrayObject('Test')).toStrictEqual(false);
        expect(isNonNullNonArrayObject([])).toStrictEqual(false);
        expect(isNonNullNonArrayObject([1, 2])).toStrictEqual(false);
    });
});
