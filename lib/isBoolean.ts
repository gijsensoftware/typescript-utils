import { BOOLEAN } from './types';

export const isBoolean = (input: unknown): input is boolean => typeof input === BOOLEAN;
