export const STRING = 'string';
export const OBJECT = 'object';
export const BOOLEAN = 'boolean';
export const UNDEFINED = 'undefined';
export const NUMBER = 'number';
