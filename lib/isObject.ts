import { OBJECT } from './types';

export const isObject = (input: unknown): input is undefined | { [key: string]: any } =>
    typeof input === OBJECT;
