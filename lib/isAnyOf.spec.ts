import { isAnyOf } from './isAnyOf';
import { valuesFromEnum } from './valueFromEnum';

enum TestEnum {
    WAARDE_A = 'WAARDE-A',
    WAARDE_B = 'WAARDE-B',
}

describe('isAnyOf', () => {
    it('should check whether array contains a value', () => {
        expect(isAnyOf('testwaarde', [1, 'testwaarde', false])).toStrictEqual(true);
    });

    it('should be case insensitive', () => {
        expect(isAnyOf('Testwaarde', [1, 'testwaarde', false])).toStrictEqual(false);
    });

    it('should not mix up types (truthy and falsy)', () => {
        expect(isAnyOf('', [1, 'testwaarde', false])).toStrictEqual(false);
        expect(isAnyOf(0, [1, 'testwaarde', false])).toStrictEqual(false);
    });

    it('should validate values for given enum', () => {
        expect(isAnyOf(TestEnum.WAARDE_A, valuesFromEnum(TestEnum))).toStrictEqual(true);
        expect(isAnyOf('WAARDE-A', valuesFromEnum(TestEnum))).toStrictEqual(true);
    });
});
