export const isAnyOf = (input: unknown, values: any[]): boolean => values.some(value => value === input)
