import { isNonNullObject } from './isNonNullObject';

describe('isNonNullObject', () => {
    it('should validate objects', () => {
        expect(isNonNullObject({})).toStrictEqual(true);
        expect(isNonNullObject({foo: 'bar'})).toStrictEqual(true);
        expect(isNonNullObject([])).toStrictEqual(true);
        expect(isNonNullObject([1, 2])).toStrictEqual(true);
    });

    it('should not validate non-objects', () => {
        expect(isNonNullObject(undefined)).toStrictEqual(false);
        expect(isNonNullObject(null)).toStrictEqual(false);
        expect(isNonNullObject(1)).toStrictEqual(false);
        expect(isNonNullObject('')).toStrictEqual(false);
        expect(isNonNullObject('Test')).toStrictEqual(false);
    });
});
