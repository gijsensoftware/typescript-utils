import { isNumber } from './isNumber';

describe('isBoolean', () => {
    it('should validate booleans', () => {
        expect(isNumber(-1)).toStrictEqual(true);
        expect(isNumber(0)).toStrictEqual(true);
        expect(isNumber(1)).toStrictEqual(true);
        expect(isNumber(0.0000001)).toStrictEqual(true);
        expect(isNumber(-0.0000001)).toStrictEqual(true);
    });

    it('should not validate non-booleans', () => {
        expect(isNumber(undefined)).toStrictEqual(false);
        expect(isNumber(null)).toStrictEqual(false);
        expect(isNumber(true)).toStrictEqual(false);
        expect(isNumber(false)).toStrictEqual(false);
        expect(isNumber('')).toStrictEqual(false);
        expect(isNumber('Test')).toStrictEqual(false);
        expect(isNumber({})).toStrictEqual(false);
        expect(isNumber([])).toStrictEqual(false);
        expect(isNumber({foo: 'bar'})).toStrictEqual(false);
        expect(isNumber([1, 2])).toStrictEqual(false);
    });
});
