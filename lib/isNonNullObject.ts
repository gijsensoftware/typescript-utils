import { OBJECT } from './types';

export const isNonNullObject = (input: unknown): input is { [key: string]: any } | Array<any> =>
    typeof input === OBJECT
    && input !== null;
