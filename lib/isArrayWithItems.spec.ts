import { isAnyOf } from './isAnyOf';
import { isArrayWithItems } from './isArrayWithItems';
import { isNonNullNonArrayObject } from './isNonNullNonArrayObject';
import { isNonNullObject } from './isNonNullObject';
import { isNumber } from './isNumber';
import { isObject } from './isObject';
import { isString } from './isString';
import { valuesFromEnum } from './valueFromEnum';

enum TestEnum {
    ValueA = 'ValueA',
    ValueB = 'ValueB',
}

const isTestEnum = (input: unknown): input is TestEnum =>
    isAnyOf(input, valuesFromEnum(TestEnum));

describe('isArrayWithItems', () => {
    it('should validate number arrays', () => {
        expect(isArrayWithItems(isNumber, [0, 1, 2, 3])).toStrictEqual(true);
    });

    it('should validate string arrays', () => {
        expect(isArrayWithItems(isString, ['0', 'a', 'b', ''])).toStrictEqual(true);
    });

    it('should validate enum arrays', () => {
        expect(isArrayWithItems(isTestEnum, [TestEnum.ValueA, TestEnum.ValueB])).toStrictEqual(true);
    });

    it('should validate complex type arrays', () => {
        expect(isArrayWithItems(isObject, [null, [], {}])).toStrictEqual(true);

        expect(isArrayWithItems(isNonNullObject, [null, {}])).toStrictEqual(false);
        expect(isArrayWithItems(isNonNullObject, [[], {}])).toStrictEqual(true);

        expect(isArrayWithItems(isNonNullNonArrayObject, [null, {}])).toStrictEqual(false);
        expect(isArrayWithItems(isNonNullNonArrayObject, [[], {}])).toStrictEqual(false);
        expect(isArrayWithItems(isNonNullNonArrayObject, [{}])).toStrictEqual(true);
    });

    it('should not validate mixed arrays', () => {
        expect(isArrayWithItems(isNumber, [0, '1', 2, 3])).toStrictEqual(false);
    });
});
