import { valuesFromEnum } from './valueFromEnum';

enum TestEnum {
    WAARDE_A = 'WAARDE-A',
    WAARDE_B = 'WAARDE-B',
}

describe('valuesFromEnum', () => {
    it('should return values from given enum', () => {
        expect(valuesFromEnum(TestEnum))
            .toStrictEqual([
                'WAARDE-A',
                'WAARDE-B',
            ]);
    });
});
