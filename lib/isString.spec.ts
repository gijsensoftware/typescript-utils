import { isString } from './isString';

describe('isString', () => {
    it('should validate booleans', () => {
        expect(isString('')).toStrictEqual(true);
        expect(isString('a')).toStrictEqual(true);
        expect(isString('a')).toStrictEqual(true);
        expect(isString(`a`)).toStrictEqual(true);
    });

    it('should not validate non-booleans', () => {
        expect(isString(undefined)).toStrictEqual(false);
        expect(isString(null)).toStrictEqual(false);
        expect(isString(true)).toStrictEqual(false);
        expect(isString(false)).toStrictEqual(false);
        expect(isString(0)).toStrictEqual(false);
        expect(isString(1)).toStrictEqual(false);
        expect(isString({})).toStrictEqual(false);
        expect(isString([])).toStrictEqual(false);
        expect(isString({foo: 'bar'})).toStrictEqual(false);
        expect(isString([1, 2])).toStrictEqual(false);
    });
});
