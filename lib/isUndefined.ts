import { UNDEFINED } from './types';

export const isUndefined = (input: unknown): input is undefined => typeof input === UNDEFINED
