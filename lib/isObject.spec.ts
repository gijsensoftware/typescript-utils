import { isObject } from './isObject';

describe('isBoolean', () => {
    it('should validate objects', () => {
        expect(isObject({})).toStrictEqual(true);
        expect(isObject({foo: 'bar'})).toStrictEqual(true);
        expect(isObject([])).toStrictEqual(true);
        expect(isObject(null)).toStrictEqual(true);
        expect(isObject([1, 2])).toStrictEqual(true);
    });

    it('should not validate non-objects', () => {
        expect(isObject(undefined)).toStrictEqual(false);
        expect(isObject(true)).toStrictEqual(false);
        expect(isObject(false)).toStrictEqual(false);
        expect(isObject(1)).toStrictEqual(false);
        expect(isObject('')).toStrictEqual(false);
        expect(isObject('Test')).toStrictEqual(false);
    });
});
