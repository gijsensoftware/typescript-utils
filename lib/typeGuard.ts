export type TypeGuard<T> = (input: unknown) => input is T;
