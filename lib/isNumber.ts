import { NUMBER } from './types';

export const isNumber = (input: unknown): input is number => typeof input === NUMBER
