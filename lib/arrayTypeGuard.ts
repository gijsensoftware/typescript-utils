import { isArrayWithItems } from './isArrayWithItems';
import { TypeGuard } from './typeGuard';

/**
 * Composes a typeguard for an array based on a typeguard for a singular array entry
 * @param guard Any typeguard
 * @returns A typeguard that checks whether the input is an array containing only items that validate according to {@param guard}
 */
export const arrayTypeGuard = <T>(guard: TypeGuard<T>): TypeGuard<T[]> =>
    (input: unknown): input is T[] => isArrayWithItems(guard, input)
