import { STRING } from './types';

export const isString = (input: unknown): input is string => typeof input === STRING;
