import { isBoolean } from './isBoolean';

describe('isBoolean', () => {
    it('should validate booleans', () => {
        expect(isBoolean(true)).toStrictEqual(true);
        expect(isBoolean(false)).toStrictEqual(true);
    });

    it('should not validate non-booleans', () => {
        expect(isBoolean(undefined)).toStrictEqual(false);
        expect(isBoolean(null)).toStrictEqual(false);
        expect(isBoolean(1)).toStrictEqual(false);
        expect(isBoolean('')).toStrictEqual(false);
        expect(isBoolean('Test')).toStrictEqual(false);
        expect(isBoolean({})).toStrictEqual(false);
        expect(isBoolean([])).toStrictEqual(false);
        expect(isBoolean({foo: 'bar'})).toStrictEqual(false);
        expect(isBoolean([1, 2])).toStrictEqual(false);
    });
});
