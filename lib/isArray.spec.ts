import { isArray } from './isArray';

describe('isArray', () => {
    it('should validate arrays', () => {
        expect(isArray([])).toStrictEqual(true);
        expect(isArray([1, 2])).toStrictEqual(true);
    });

    it('should not validate non-arrays', () => {
        expect(isArray(undefined)).toStrictEqual(false);
        expect(isArray(null)).toStrictEqual(false);
        expect(isArray(1)).toStrictEqual(false);
        expect(isArray('')).toStrictEqual(false);
        expect(isArray('Test')).toStrictEqual(false);
        expect(isArray({})).toStrictEqual(false);
        expect(isArray({foo: 'bar'})).toStrictEqual(false);
    });
});
