import { OBJECT } from './types';

export const isArray = (input: unknown): input is unknown[] =>
    typeof input === OBJECT
    && Array.isArray(input);
